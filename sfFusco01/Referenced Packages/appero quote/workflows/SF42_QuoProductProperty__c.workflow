<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Produkteigenschaften_automated_false</fullName>
        <field>SF42_prop_Automated__c</field>
        <literalValue>0</literalValue>
        <name>Produkteigenschaften automated false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Produkteigenschaften_automated_true</fullName>
        <description>Aktivierung der Checkbox &quot;automated&quot; in den Produkteigenschaften</description>
        <field>SF42_prop_Automated__c</field>
        <literalValue>1</literalValue>
        <name>Produkteigenschaften automated true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Produkteigenschaften_bulkprice_true</fullName>
        <field>SF42_isBulkPrice__c</field>
        <literalValue>1</literalValue>
        <name>Produkteigenschaften Bulkprice true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Preiskalkulation automated</fullName>
        <actions>
            <name>Produkteigenschaften_automated_true</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SF42_QuoProductProperty__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Preiskalkulation</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Produkteigenschaften nicht automatisch</fullName>
        <actions>
            <name>Produkteigenschaften_automated_false</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SF42_QuoProductProperty__c.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Preiskalkulation</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Staffelpreise</fullName>
        <actions>
            <name>Produkteigenschaften_bulkprice_true</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SF42_QuoProductProperty__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Staffelpreisberechnung,Preiskalkulation aus Summe</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
