/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class SF42_Webservice {
    global SF42_Webservice() {

    }
    webService static String GetPricebookEntryID(String PbId) {
        return null;
    }
    webService static String GetPricebookEntryIDCur(String PbId, String CurrencyIsoCodeValue) {
        return null;
    }
}
