/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class Utils {
    global Utils() {

    }
    global static List<String> getFieldsAccessible(String objName, List<String> fieldList) {
        return null;
    }
    global static Map<String,String> getFieldsLabels(String objName, List<String> fieldList) {
        return null;
    }
    global static Map<String,qe.Utils.Numeric_Label> getFieldsNumeric_Label(String objName, List<String> fieldList) {
        return null;
    }
    global static Map<String,Boolean> getFieldsNumeric(String objName, List<String> fieldList) {
        return null;
    }
    global static Map<String,String> getFieldsTypes(String objName, List<String> fieldList) {
        return null;
    }
    global static Map<String,String> getFieldsUpdatable(String objName, List<String> fieldList) {
        return null;
    }
    global static Map<String,String> getFieldsUpdatable(String objName, List<String> fieldList, Map<String,String> resultFieldTypes) {
        return null;
    }
    global static List<System.SelectOption> getFolders() {
        return null;
    }
    global static String getHost() {
        return null;
    }
    global static String getVFHost() {
        return null;
    }
global class Field implements System.Comparable {
    global Integer compareTo(Object o) {
        return null;
    }
}
global class Numeric_Label {
}
}
