/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class Settings {
    global String getLanguageDescriptions() {
        return null;
    }
    global static String getLanguageDescriptionsStatic() {
        return null;
    }
    global static String setConfig(String name, String value) {
        return null;
    }
    global String setLanguageDescription(String languageFields, String languageQuoteField, String languageDescriptionField) {
        return null;
    }
    global static String setLanguageDescriptionStatic(String languageFields, String languageQuoteField, String languageDescriptionField) {
        return null;
    }
}
