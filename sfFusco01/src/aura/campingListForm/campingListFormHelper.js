({
	validateItemForm: function(component) {
        // Simplistic error checking
        var valid = true;

        // Name must not be blank
        var nameField = component.find("ci_name");
        var ciName = nameField.get("v.value");
        if($A.util.isEmpty(ciName)) {
            valid = false;
            nameField.set("v.errors", [{message:"Camping Item Name can't be blank."}]);
        }
        else {
            nameField.set("v.errors", null);
        }
        
        // Quantity must not be blank
        var qtyField = component.find("ci_quantity");
        var ciQty = qtyField.get("v.value");
        if($A.util.isEmpty(ciQty)) {
            valid = false;
            qtyField.set("v.errors", [{message:"Quantity can't be blank."}]);
        }
        else {
            qtyField.set("v.errors", null);
        }
        
        // Price must not be blank
        var priceField = component.find("ci_price");
        var ciPrice = priceField.get("v.value");
        if($A.util.isEmpty(ciPrice)) {
            valid = false;
            priceField.set("v.errors", [{message:"Price can't be blank."}]);
        }
        else {
            priceField.set("v.errors", null);
        }
        
        return(valid);
    },
    
    createItem: function(component, newItem) {
        var createEvent = component.getEvent("addItem");
        createEvent.setParams({ "item": newItem });
        createEvent.fire();
        
        component.set("v.newItem", {'sobjectType':'Camping_Item__c', 'Packed__c':false, 'Name':'Test', 'Quantity__c':0, 'Price__c':0});
    }

})