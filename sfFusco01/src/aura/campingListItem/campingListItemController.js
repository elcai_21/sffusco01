({
	packItem: function(component, event, helper) {
        var item = component.get("v.item");
 		item.Packed__c = true;
        component.set("v.newItem", item);
		var btnClicked = event.getSource();
 		btnClicked.set("v.disabled", true);
	}
})