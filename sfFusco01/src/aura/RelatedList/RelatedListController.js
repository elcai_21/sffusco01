/**
 * Created by g.rota on 10/04/2017.
 */
 ({
    doInit : function (component,event,helper) {
        component.set("v.errore","");
        component.set("v.listaCampi",null);
        component.set("v.infoOrdinamento",undefined);

        var arrayCampi = component.get("v.campi").split(",");
        var scambioEseguito = false;
        for (var i=0;i<arrayCampi.length;i++)
        {
            if ( (arrayCampi[i]=="Name") ||  (arrayCampi[i]=="Id" && arrayCampi.indexOf("Name")==-1))
            {
                var tmp = arrayCampi[0];
                arrayCampi[0] = arrayCampi[i];
                arrayCampi[i] = tmp;
                scambioEseguito=true;
                break;
            }
        }
        if (!scambioEseguito)
        {
            arrayCampi.unshift("Name");
        }
        component.set("v.campi",arrayCampi.toString());
        
        // controlla se ci sono errori ed inizializza la lista, prende anche label e tipi campi
        helper.checkPermissionsAndErrors(component,helper);
    },
    createRelatedRecord : function (component,event,helper) {
        var defaultLookup = {};
        defaultLookup[component.get("v.nomeCampoRelazione")] = component.get("v.recordId");
        var navEvt = $A.get("e.force:createRecord");
        if (component.get("v.idRecordType").length>0)
        navEvt.setParams({
            "entityApiName": component.get("v.nomeOggettoCorrelato"),
            "recordTypeId": component.get("v.idRecordType")[0],
            "defaultFieldValues": defaultLookup
        });
        else
        navEvt.setParams({
            "entityApiName": component.get("v.nomeOggettoCorrelato"),
            "defaultFieldValues": defaultLookup
        });
        navEvt.fire();
    },
    viewRelatedList : function (component,event,helper) {
        var navEvt = $A.get("e.force:navigateToRelatedList");
        navEvt.setParams({
            "parentRecordId": component.get("v.recordId"),
            "relatedListId": component.get("v.relationshipName")
        });
        navEvt.fire();
    },
    ordinaRecord : function (component,event,helper) {
        var el = event.target;
        while (el.nodeName!="TH")
        {
            el = el.parentNode;
        }
        var nomeCampoDaOrdinare = el.getAttribute("title");
        var nomiCampiAPI = component.get("v.campi").split(",");
        var listaCampi = component.get("v.listaCampi");
        var nomeAPICampoDaOrdinare = nomiCampiAPI[listaCampi.indexOf(nomeCampoDaOrdinare)];
        var ultimoCampoOrdinato;
        var ordine;
        if (component.get("v.infoOrdinamento")!=undefined)
        {
            ultimoCampoOrdinato = component.get("v.infoOrdinamento").nomeCampo;
            ordine = component.get("v.infoOrdinamento").ordine;
        }
        else
        {
            ultimoCampoOrdinato = null;
        }
        
        if (ultimoCampoOrdinato==nomeCampoDaOrdinare) ordine = (ordine=="ASC") ? "DESC" : "ASC";
        else ordine="ASC";
        var objOrdine = {
            "nomeCampo":nomeCampoDaOrdinare,
            "nomeCampoAPI":nomeAPICampoDaOrdinare,
            "ordine":ordine
        };
        helper.getRelatedObjects(component,helper,objOrdine);
    },
    ordinaRecordModTile : function (component,event,helper) {
        var nomeCampoDaOrdinare = component.find("selectCampoDaOrdinare").get("v.value");
        var nomiCampiAPI = component.get("v.campi").split(",");
        var listaCampi = component.get("v.listaCampi");
        var nomeAPICampoDaOrdinare = nomiCampiAPI[listaCampi.indexOf(nomeCampoDaOrdinare)];
        var ultimoCampoOrdinato;
        var ordine;
        if (component.get("v.infoOrdinamento")!=undefined)
        {
            ultimoCampoOrdinato = component.get("v.infoOrdinamento").nomeCampo;
            ordine = component.get("v.infoOrdinamento").ordine;
        }
        else
        {
            ultimoCampoOrdinato = null;
        }
        
        if (ultimoCampoOrdinato==nomeCampoDaOrdinare) ordine = (ordine=="ASC") ? "DESC" : "ASC";
        else ordine="ASC";
        var objOrdine = {
            "nomeCampo":nomeCampoDaOrdinare,
            "nomeCampoAPI":nomeAPICampoDaOrdinare,
            "ordine":ordine
        };
        helper.getRelatedObjects(component,helper,objOrdine);
    },
    deleteRecord : function(component, event, helper) {
        component.set("v.mostraModal",false);
        var idOggDaEliminare = component.get("v.recordDaEliminare").id;
        var action = component.get("c.deleteRecordById");
        action.setParams({
            "idOgg": idOggDaEliminare
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {

                var list = component.get("v.infoRecordCorrelati");
                for (var i=0;i<list.length;i++)
                {
                    if (list[i][0]["idLookup"]==idOggDaEliminare)
                    {
                        list.splice(i,1);
                        break;
                    }
                }
                component.set("v.infoRecordCorrelati",list);

                var listRecCorrelati = component.get("v.recordCorrelati");
                for (var i=0;i<listRecCorrelati.length;i++)
                {
                    if (listRecCorrelati[i]["Id"]==idOggDaEliminare)
                    {
                        listRecCorrelati.splice(i,1);
                        break;
                    }
                }
                component.set("v.recordCorrelati",listRecCorrelati);

                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Record eliminato",
                    "message": "Il record è stato eliminato con successo",
                    "type": "info",
                    "duration": "3000"
                });
                toastEvent.fire();
            }
            else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Errore",
                    "message": "Errore durante l'eliminazione del record",
                    "type": "error",
                    "duration": "3000"
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
    },
    chiudiModal : function(component, event, helper) {
        component.set("v.mostraModal",false);
    },
    handleAttemptDeleteSObject : function(component, event, helper) {
        var recordDaEliminare = {
            "id":event.getParam("idSObject"),
            "nome":event.getParam("nomeOIdSObject")
        };
        component.set("v.recordDaEliminare",recordDaEliminare);
        component.set("v.mostraModal",true);
    },
    setLibPronta : function(component, event, helper) {
        component.set("v.libPronta",true);
        helper.initPushTopic(component,helper);
    },
    doneRendering : function(component,event,helper) {
        if (component.get("v.libPronta"))
        $("#recordTable").colResizable({
            liveDrag:true
        });
    }
})