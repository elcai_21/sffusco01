/**
 * Created by g.rota on 10/04/2017.
 */
({
    getRelatedObjects : function(component,helper,infoOrdinamento)
    {
        if (infoOrdinamento==undefined) {
            var action = component.get("c.getRelatedObjects");
            action.setParams({
                "nomeOgg": component.get("v.nomeOggettoCorrelato"),
                "stringaCampi": component.get("v.campi"),
                "nomeCampoRelazione": component.get("v.nomeCampoRelazione"),
                "recordId": component.get("v.recordId"),
                "limite": component.get("v.limiteNRecord")
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (component.isValid() && state === "SUCCESS") {
                    component.set("v.recordCorrelati", response.getReturnValue()[0]);
                    var permessi = response.getReturnValue()[1];
                    var permessiAlleggeriti = new Array();
                    var colonnaAzioniNecessaria = false;
                    for (var i=0;i<permessi.length;i++)
                    {
                        if (permessi[i].HasEditAccess || permessi[i].HasDeleteAccess)
                            colonnaAzioniNecessaria = true;
                        permessiAlleggeriti.push({
                            "modifica":permessi[i].HasEditAccess,
                            "elimina":permessi[i].HasDeleteAccess,
                        });
                    }
                    component.set("v.permessiRecordCorrelati",permessiAlleggeriti);
                    component.set("v.colonnaAzioniNecessaria",colonnaAzioniNecessaria);
                    component.set("v.numRecord", response.getReturnValue()[2]);

                    helper.generaOggettiInfoRecord(component,undefined);
                }
                else {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " +
                                errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });
            $A.enqueueAction(action);
        }
        else
        {
            var action = component.get("c.getRelatedSortedObjects");
            action.setStorable();
            action.setParams({
                "nomeOgg": component.get("v.nomeOggettoCorrelato"),
                "stringaCampi": component.get("v.campi"),
                "nomeCampoRelazione": component.get("v.nomeCampoRelazione"),
                "recordId": component.get("v.recordId"),
                "campoDaOrdinare": infoOrdinamento["nomeCampoAPI"],
                "ordine": infoOrdinamento["ordine"],
                "limite": component.get("v.limiteNRecord")
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (component.isValid() && state === "SUCCESS") {
                    component.set("v.recordCorrelati", response.getReturnValue()[0]);
                    var permessi = response.getReturnValue()[1];
                    var permessiAlleggeriti = new Array();
                    for (var i=0;i<permessi.length;i++)
                    {
                        permessiAlleggeriti.push({
                            "modifica":permessi[i].HasEditAccess,
                            "elimina":permessi[i].HasDeleteAccess,
                        });
                    }
                    component.set("v.permessiRecordCorrelati",permessiAlleggeriti);

                    helper.generaOggettiInfoRecord(component,infoOrdinamento);
                }
                else {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " +
                                errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });
            $A.enqueueAction(action);
        }
    },
    generaOggettiInfoRecord : function (component,objOrdinamento) {
        var recordCorrelati = component.get("v.recordCorrelati");
        var permessiRecord = component.get("v.permessiRecordCorrelati");
        var tmp_nomiCampi = component.get("v.listaCampi");
        var tmp_tipiCampi = component.get("v.listaTipiCampi");
        var nomiCampiAPI = component.get("v.campi").split(",");
        var etichettaPlurale = component.get("v.etichettaPlurale");
        var modVisualizzazione = component.get("v.modVisualizzazione");

        var nomiCampi = new Array(); //LABEL
        for (var i=0;i<tmp_nomiCampi.length;i++) nomiCampi.push(tmp_nomiCampi[i]);
        var tipiCampi = new Array();
        for (var i=0;i<tmp_tipiCampi.length;i++) tipiCampi.push(tmp_tipiCampi[i]);
        //GENERAZIONE OGGETTO SUPREMO
        var infoRecord = new Array();
        tipiCampi[0] = 'REFERENCE';

        var stringhePerLookup = new Array();
        for (var i=0;i<recordCorrelati.length;i++)
        {
            var iRiga = new Array();
            for (var j=0;j<nomiCampi.length;j++)
            {
                var iCampo;
                var valore = recordCorrelati[i][nomiCampiAPI[j]];
                if (nomiCampiAPI[j].indexOf(".")!=-1)
                {
                    var copiaNomeCampo = nomiCampiAPI[j];
                    var oggConCampo = undefined;
                    var nessunValore = false;
                    while (copiaNomeCampo.indexOf(".")!=-1)
                    {
                        var nomeCampo = copiaNomeCampo.substr(0,copiaNomeCampo.indexOf("."));
                        if (oggConCampo==undefined) oggConCampo = recordCorrelati[i][nomeCampo];
                        else oggConCampo = oggConCampo[nomeCampo];
                        copiaNomeCampo = copiaNomeCampo.substr( copiaNomeCampo.indexOf(".")+1, copiaNomeCampo.length-copiaNomeCampo.indexOf(".")-1 );
                        if (oggConCampo==undefined) 
                        {
                            nessunValore = true;
                            break;
                        }
                    }
                    if (!nessunValore) valore = oggConCampo[copiaNomeCampo];
                    else valore = "";

                    if (tipiCampi[j]=="REFERENCE" && valore!="" && valore!=undefined)
                    {
                        stringhePerLookup.push(i+","+nomiCampi[j]+","+valore);
                    }
                }
                else
                {
                    if (tipiCampi[j]=="REFERENCE" && recordCorrelati[i][nomiCampiAPI[j]]!=undefined && nomiCampiAPI[j]!="Id" && nomiCampiAPI[j]!="Name")
                    {
                        stringhePerLookup.push(i+","+nomiCampi[j]+","+recordCorrelati[i][nomiCampiAPI[j]]);
                    }
                }

                if ( (nomiCampiAPI[j]=="Name") || (nomiCampiAPI[j]=="Id" && nomiCampiAPI.indexOf("Name")==-1) )
                {
                    iCampo = {
                        "nome": nomiCampi[j],
                        "tipo": tipiCampi[j],
                        "valore": valore,
                        "idLookup": null,
                        "modifica":permessiRecord[i].modifica,
                        "elimina":permessiRecord[i].elimina
                    };
                }
                else
                {
                    iCampo = {
                        "nome": nomiCampi[j],
                        "tipo": tipiCampi[j],
                        "valore": valore,
                        "idLookup": null
                    };
                }
                if (nomiCampiAPI[j]=="Id" || nomiCampiAPI[j]=="Name")
                {
                    iCampo.idLookup = recordCorrelati[i]["Id"];
                }
                iRiga.push(iCampo);
            }
            infoRecord.push(iRiga);
        }
        //QUERY idLookup
        if (stringhePerLookup.length>0) {
            var action = component.get("c.getNamesFromId");
            action.setStorable();
            action.setParams({
                "reqName": stringhePerLookup
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (component.isValid() && state === "SUCCESS") {
                    var campiDaAggiornare = response.getReturnValue();
                    for (var i = 0; i < campiDaAggiornare.length; i++) {
                        var p = campiDaAggiornare[i].split(",");
                        var nRecord = p[0];
                        var nomeCampo = p[1];
                        var nomeRestituito = p[2];
                        var c = 0;
                        var indexCampo = null;
                        while (indexCampo == null) {
                            if (nomiCampi[c] == nomeCampo) indexCampo = c;
                            c++;
                        }
                        var idLookup = infoRecord[nRecord][indexCampo]["valore"];
                        infoRecord[nRecord][indexCampo]["valore"] = nomeRestituito;
                        infoRecord[nRecord][indexCampo]["idLookup"] = idLookup;
                    }
                    component.set("v.infoRecordCorrelati", infoRecord);
                    if (objOrdinamento!=undefined) {
                        component.set("v.infoOrdinamento", objOrdinamento);
                    }
                }
                else {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " +
                                errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                    console.log("Failed with state: " + state);
                }
            });
            $A.enqueueAction(action);
        }
        else
        {
            component.set("v.infoRecordCorrelati", infoRecord);
            if (objOrdinamento!=undefined)
                component.set("v.infoOrdinamento",objOrdinamento);
        }
    },
    checkPermissionsAndErrors : function (component,helper) {
        var action = component.get("c.getPermissionsAndErrors");
        action.setParams({
            "nomeOggetto": component.get("v.nomeOggettoCorrelato"),
            "stringaCampi": component.get("v.campi"),
            "nomeCampoRelazione": component.get("v.nomeCampoRelazione"),
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                if (!response.getReturnValue()["CampiENomeOggValidi"]) {
                    component.set("v.errore", "Errore nei campi o nel nome oggetto. Controlla che il nome dell'oggetto ed i nomi dei campi siano scritti correttamente.");
                }
                else
                {
                    var leggi = true;
                    var crea = true;
                    var permessiCampi = response.getReturnValue().Campi;
                    var campiNonAccessibili = new Array();
                    for (var i=0;i<permessiCampi.length;i++)
                    {
                        if (!permessiCampi[i])
                            campiNonAccessibili.push(i);
                    }
                    if (campiNonAccessibili.length==permessiCampi.length) {
                        leggi = false;
                        crea = false;
                        var objPermessi = {
                            "leggi": leggi,
                            "crea": crea
                        };
                        component.set("v.infoPermessi",objPermessi);
                    }
                    else
                    {
                        var listaLabel = response.getReturnValue()["Label"];
                        var listaTipi = response.getReturnValue()["Tipi"];

                        var campi = component.get("v.campi").split(",");
                        for (var i=(campi.length-1);i>=0;i--)
                        {
                            if (campiNonAccessibili.indexOf(i)!=-1)
                            {
                                campi.splice(i,1);
                                listaLabel.splice(i,1);
                                listaTipi.splice(i,1);
                            }
                        }
                        component.set("v.campi",campi.toString());
                        component.set("v.listaCampi",listaLabel);
                        component.set("v.listaTipiCampi",listaTipi);

                        crea = response.getReturnValue().Oggetto;

                        var objPermessi = {
                            "leggi": leggi,
                            "crea": crea
                        };

                        helper.getPluralLabelAndRelationshipName(component,helper,objPermessi);

                        helper.getIcon(component);

                        helper.getRecordTypes(component);
                    }
                }
            }
            else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    getLabelsAndFieldsType : function (component,helper) {
        var strCampi = component.get("v.campi");
        var arrayCampi = strCampi.split(",");
        var action = component.get("c.getLabelsAndFieldsType");
        action.setParams({
            "nomeOgg":component.get("v.nomeOggettoCorrelato"),
            "stringaCampi": strCampi
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.listaCampi",response.getReturnValue()[0]);
                component.set("v.listaTipiCampi",response.getReturnValue()[1]);
                helper.getRelatedObjects(component,helper,undefined);
                helper.initPushTopic(component,helper);
            }
            else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    getPluralLabelAndRelationshipName : function (component,helper,oggPermessi) {
        var action = component.get("c.getPluralLabelAndRelationshipName");
        action.setParams({
            "nomeOgg":component.get("v.nomeOggettoCorrelato"),
            "nomeCampoRelazione": component.get("v.nomeCampoRelazione"),
            "recordId": component.get("v.recordId")
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                if (response.getReturnValue()==null)
                {
                    component.set("v.errore","Non esiste alcuna relazione con l'oggetto ed il campo di relazione indicati");
                }
                else
                {
                    component.set("v.infoPermessi",oggPermessi);
                    component.set("v.etichettaPlurale",response.getReturnValue()[0]);
                    component.set("v.relationshipName",response.getReturnValue()[1]);

                    helper.getRelatedObjects(component,helper,undefined);
                    helper.initPushTopic(component,helper);
                }
            }
            else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    getIcon : function (component) {
        var action = component.get("c.getIcon");
        action.setParams({
            "nomeOggetto":component.get("v.nomeOggettoCorrelato"),
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var iconName = response.getReturnValue();
                if (iconName!=null) component.set("v.icona",iconName);
            }
            else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    getRecordTypes : function(component) {
        var action = component.get("c.getAvailableRecordTypes");
        action.setParams({
            "nomeOgg":component.get("v.nomeOggettoCorrelato"),
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.idRecordType",response.getReturnValue());
            }
            else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    initPushTopic : function(component, helper) {
        var libPronta = component.get("v.libPronta");
        var tipiCampi = component.get("v.listaTipiCampi");
        if (libPronta && tipiCampi.length>0)
        {
            //elimino text area in quanto non supportate da Streaming API
            var nomiCampiAPI = component.get("v.campi").split(",");
            
            var stringaCampiSenzaTextArea = "";
            for (var i=0;i<nomiCampiAPI.length;i++)
            {
                if (tipiCampi[i+1]!="TEXTAREA") stringaCampiSenzaTextArea += nomiCampiAPI[i]+",";
            }
            stringaCampiSenzaTextArea = stringaCampiSenzaTextArea.substr(0,stringaCampiSenzaTextArea.length-1);
            var nomePushTopic = component.get("v.nomePushTopic");
            var action = component.get("c.getSessionIdAndInitPushTopic");
            action.setParams({
                "nomeOgg": component.get("v.nomeOggettoCorrelato"),
                "stringaCampi": stringaCampiSenzaTextArea,
                "nomeCampoRelazione": component.get("v.nomeCampoRelazione"),
                "recordId": component.get("v.recordId"),
                "nomePushTopic": nomePushTopic
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (component.isValid() && state === "SUCCESS") {
                    var sessionId = response.getReturnValue();
                    var authstring = "OAuth " + sessionId;
                    $.cometd.init({
                        url: window.location.protocol + '//' + window.location.hostname + '/cometd/40.0/',
                        requestHeaders: { Authorization: authstring },
                        appendMessageTypeToURL : false
                    });
                    $.cometd.subscribe('/topic/'+nomePushTopic, function (message){
                        console.log("Uh?!");
                        var infoRecAggiornato = message.data.sobject;
                        switch (message.data.event.type)
                        {
                            case "updated":
                                /*
                                var recCorrelati = component.get("v.recordCorrelati");
                                var recAggiornato;
                                for (var i=0;i<recCorrelati.length;i++)
                                {
                                    if (recCorrelati[i].Id==infoRecAggiornato.Id)
                                    {
                                        recAggiornato = recCorrelati[i];
                                        for (var nomeCampo in infoRecAggiornato)
                                        {
                                            recAggiornato[nomeCampo] = infoRecAggiornato[nomeCampo];
                                        }
                                        break;
                                    }
                                }
                                component.set("v.recordCorrelati",recCorrelati);
                                var infoOrdinamento = component.get("v.infoOrdinamento");
                                helper.generaOggettiInfoRecord(component,infoOrdinamento);
                                */
                                var infoOrdinamento = component.get("v.infoOrdinamento");
                                helper.getRelatedObjects(component,helper,infoOrdinamento);
                                break;
                            case "deleted":
                                var infoRecCorrelati = component.get("v.infoRecordCorrelati");
                                for (var i = 0; i < infoRecCorrelati.length; i++) {
                                    if (infoRecCorrelati[i][0].idLookup==infoRecAggiornato.Id)
                                    {
                                        infoRecCorrelati.splice(i,1);
                                        break;
                                    }
                                }
                                component.set("v.infoRecordCorrelati",infoRecCorrelati);
                                var nRecord = component.get("v.numRecord");
                                nRecord--;
                                component.set("v.numRecord",nRecord);
                                break;
                            case "created":
                                var infoOrdinamento = component.get("v.infoOrdinamento");
                                helper.getRelatedObjects(component,helper,infoOrdinamento);
                                var nRecord = component.get("v.numRecord");
                                nRecord++;
                                component.set("v.numRecord",nRecord);
                                break;
                        }
                    });
                }
                else {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " +
                                errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });
            $A.enqueueAction(action);
        }
    }
})