({
	doInit : function (component,event,helper) {
		var infoRecordCorrelato = component.get("v.infoRecordCorrelato");
        component.set("v.recordId",infoRecordCorrelato[0].idLookup);
        component.set("v.identificativoRecord",infoRecordCorrelato[0].valore);
    },
	editRecord : function(component, event, helper) {
        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
            "recordId": component.get("v.recordId")
        });
        editRecordEvent.fire();
    },
    confirmDeleteRecord : function(component, event, helper) {
        var evt = component.getEvent("AttemptDeleteSObject");
    	evt.setParams({
    		"idSObject":component.get("v.recordId"),
    		"nomeOIdSObject":component.get("v.identificativoRecord")
    	});
    	evt.fire();
    },
    navigateToRecordDetail : function (component,event,helper) {
    	var id = event.target.getAttribute("data-recordid");
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": id
        });
        navEvt.fire();
    },
})