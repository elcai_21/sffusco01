({    
    // Load expenses from Salesforce
    doInit: function(component, event, helper) {
        // Create the action
        var action = component.get("c.getItems");
    
        // Add callback behavior for when response is received
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.items", response.getReturnValue());
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
    
        // Send action off to be executed
        $A.enqueueAction(action);
	},
    
	/*clickPacked: function(component, event, helper) {
        var expense = component.get("v.item");
        var updateEvent = component.getEvent("addItem");
        updateEvent.setParams({ "item": item });
        updateEvent.fire();
    },*/
    
    handleAddItem: function(component, event, helper) {
    	var newItem = event.getParam("item");
    	createItem(component, newItem);
	},
    
    createItem : function(component, camping) {
        var allCampings = component.get("v.items");
        var newCamping = JSON.parse(JSON.stringify(camping));
    
        // Set the sobjectType!
        camping.sobjectType = 'Camping_Item__c';
    
        var action = component.get("c.saveItem");
        action.setParams({"item" : camping});
    
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                allCampings.push(newCamping);
                component.set("v.items", allCampings);
            } else if(state === "ERROR") {
                var errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    } else {
                        console.log("Unknown Error");
                    }
                }
            }
        });        
    
        $A.enqueueAction(action);
    }

})