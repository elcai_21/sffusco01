global without sharing class CampagnaTLSBollettaMailController {
    
    global Flow.Interview.CampagnaTLSBollettaMail CampagnaTLSBollettaMailFlow {get; set;}
    global Id campaignId {get; set;}
    global Id leadId {get; set;}
    
    global CampagnaTLSBollettaMailController() {
        if (Apexpages.currentPage().getParameters().get('cmId') != null || Test.isRunningTest()) {
            String cmId;
            if (Test.isRunningTest()) {
                cmId = [Select Id from CampaignMember where Id != null limit 1].Id;
            } else {
                cmId = Apexpages.currentPage().getParameters().get('cmId');
            }
            List<CampaignMember> cm = [Select Id, CampaignId, LeadId from CampaignMember where Id != null and Id = :cmId];
            if (!cm.isEmpty()) {
                this.campaignId = cm[0].CampaignId;
                this.leadId = cm[0].LeadId;
            }
        } 
    }
    
    global String getFinishPage() {
        String opId = '';
        String opNum = '';
        if (CampagnaTLSBollettaMailFlow != null) {
            opId = CampagnaTLSBollettaMailFlow.CTB_OperatoreId;
            opNum = CampagnaTLSBollettaMailFlow.CTB_OperatoreNumero;
        }
        String url = '/apex/CampagnaTLSBollettaMailLanding?cId=' + this.campaignId + '&opId=' + opId + '&opNum=' + opNum;
        system.debug('@@@@@ FinishPage = ' + url);
        //PageReference page = new PageReference(url);
        //page.setRedirect(true);
        //return page;
        return url;
    }
}