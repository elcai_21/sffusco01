/*
	Autore: Paolo Sarais
	Descrizione: Raccoglie i metodi di utilità per la gestione del VoiperCCS
*/

public without sharing class Utils_VoiperCCS {

	public static String getIntegrationSession(String u, String p) {
		string sessionID;
		String loginUrl = '';
        
		if(Utils_VoiperCCS.isPROD()){ //Keep the setting on whether this is a production or sandbox in a custom object.<br>
		     loginURL = 'https://login.salesforce.com/services/Soap/u/26.0';
		} else {
		     loginURL = 'https://test.salesforce.com/services/Soap/u/26.0';
		}

		HttpRequest request = new HttpRequest();
		request.setEndpoint(loginURL);
		request.setMethod('POST');
		request.setHeader('Content-Type', 'text/xml;charset=UTF-8');
		request.setHeader('SOAPAction', '""');
		//not escaping username and password because we're setting those variables above
		//in other words, this line "trusts" the lines above
		//if username and password were sourced elsewhere, they'd need to be escaped below
		request.setBody('<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/"><Header/><Body><login xmlns="urn:partner.soap.sforce.com"><username>' + u + '</username><password>' + p + '</password></login></Body></Envelope>');
		system.debug('request>>>'+request);
		Dom.XmlNode resultElmt = (new Http()).send(request).getBodyDocument().getRootElement()
		.getChildElement('Body','http://schemas.xmlsoap.org/soap/envelope/')
		.getChildElement('loginResponse','urn:partner.soap.sforce.com')
		.getChildElement('result','urn:partner.soap.sforce.com');

		String SERVER_URL = resultElmt.getChildElement('serverUrl','urn:partner.soap.sforce.com').getText().split('/services')[0];
		sessionID = resultElmt.getChildElement('sessionId','urn:partner.soap.sforce.com').getText();
  		system.debug('resultElmt>>>'+resultElmt);
		return sessionID;
	}

	public static boolean isPROD(){
		String IdOrgPROD = '00Db0000000bpz0EAA';
		if(UserInfo.getOrganizationId() == IdOrgPROD) {
			return true;
		}
        return false;
	}

}