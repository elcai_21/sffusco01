/*
*	Author:			Roberto Fusco
*	Data:			19/12/2013
*	Description:	Custom controller for CreateLineItems.page.
*					An opportunity can already have line items or not; if line items are already created they are loaded,
*					otherwise there is the possibility to create new line items from zero.
*					A user can add, remove and edit line items loaded or newly created.
*					Line items are stored in a list of LineItem objects, which contain all the information needed.
*					At the end the DB is updated with the content of the list.
*/

public with sharing class CreateLineItems {
	
	private List<OpportunityLineItem> oliLoaded;
	private List<OpportunityLineItem> toDelete;
	public final Opportunity opportunity {get;set;}
	public final Boolean editing {get;set;}
	public final String business {get;set;}
	public List<LineItem> liList {get;set;}
	public Integer totalRows {get;set;}
	public Integer selected {get;set;}
	public Decimal totalAmount {get;set;}
	public String userLocale {get;set;}
	
	//Constructor
	public CreateLineItems(ApexPages.StandardController controller) {
		String oppId = Apexpages.currentPage().getParameters().get('oppid');
		editing = Boolean.valueOf(Apexpages.currentPage().getParameters().get('edit'));
		userLocale = [SELECT LocaleSidKey FROM User WHERE Id=:UserInfo.getUserId() LIMIT 1].LocaleSidKey;
		liList = new List<LineItem>();
		toDelete = new List<OpportunityLineItem>();
		
		opportunity = [SELECT Name, AccountId, OwnerId, StageName, CloseDate, Pricebook2Id, Business__c, Probability,
		Funnel_phase__c, Office_reference__c, Sales_Organization__c, Account.OwnerId FROM Opportunity WHERE Id = :oppId];
		
		if(opportunity == null)
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'An error occurred while loading the opportunity'));
		else {
			if(editing) {
				oliLoaded = [SELECT PricebookEntryId, Quantity, UnitPrice, TotalPrice, Category__c, Family__c, Series__c, ServiceDate, Description FROM OpportunityLineItem WHERE OpportunityId = :opportunity.Id];
				if(oliLoaded.size() < 1)
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'An error occurred while loading line items'));
				else {
					//Initialize fields
					totalRows = oliLoaded.size()-1;
					
					for(Integer i=0; i<oliLoaded.size(); i++) {
						liList.add(new LineItem(oliLoaded.get(i), i));
					}
				}
			} else {
				totalRows = 0;
				liList.add(new LineItem(opportunity.Id, 0));
			}
			
			business = opportunity.Business__c;
			if(business=='' || business==null)
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.CLI_ERR_Business));
			
			if(opportunity.Pricebook2Id == null) {
				Pricebook2 pb = [SELECT Id FROM Pricebook2 WHERE IsStandard=true AND IsActive=true];
				if(pb == null)
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'The standard Pricebook is not active, please activate it to continue.'));
				else
					opportunity.Pricebook2Id = pb.Id;
			}
		}
	}
	
	public void addLineItem() {
		System.debug(LoggingLevel.ERROR, 'DEBUGRF add() total_rows: ' + totalRows);
		totalRows++;
		LineItem li = new LineItem(opportunity.Id, totalRows);
        liList.add(li);
	}
	
	public void removeLineItem() {
		System.debug(LoggingLevel.ERROR, 'DEBUGRF remove() selected_row: ' + selected);
		LineItem deleted = liList.remove(selected);
		if(deleted.initialSeries != '')
			toDelete.add(deleted.oli);
		
		//Update indexes of Line Items
		for(Integer i=0; i<liList.size(); i++) {
			liList.get(i).index = i;
		}
		totalRows--;
		System.debug(LoggingLevel.ERROR, 'DEBUGRF remove() total_rows: ' + totalRows);
	}
	
	//Called when the 'Save' button is clicked from the Visualforce page
    public PageReference save() {
        try {
        	System.debug(LoggingLevel.ERROR, 'DEBUGRF save()');
        	
        	List<OpportunityLineItem> toInsert = new List<OpportunityLineItem>();
        	List<OpportunityLineItem> toUpdate = new List<OpportunityLineItem>();

        	for(LineItem li : liList) {
        		
        		if(li.oli.Quantity <= 0)
	        		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.CLI_ERR_Quantity));
	        	if(li.oli.UnitPrice <= 0)
	       			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.CLI_ERR_Unitprice));
	    		
	    		if(li.oli.Quantity>0 && li.oli.UnitPrice>0) {
	        		if(editing) {
	        			//Need to update
	        			System.debug(LoggingLevel.ERROR, 'DEBUGRF li.initialSeries/li.oli.Series__c: ' + li.initialSeries + '/' + li.oli.Series__c);
		        		if(li.initialSeries.equals(li.oli.Series__c)) {
				        	System.debug(LoggingLevel.ERROR, 'DEBUGRF quantity/unitprice: ' + li.oli.Quantity + '/' + li.oli.UnitPrice);
			        		toUpdate.add(li.oli);
			        	//Need to delete and create a new line item
		        		} else {
		        			System.debug(LoggingLevel.ERROR, 'DEBUGRF oli.Series__c: ' + li.oli.Series__c);
		        			Product2 product;
				        	if(li.oli.Series__c != '')
				        		product = [SELECT Id FROM Product2 WHERE Name=:li.oli.Series__c AND IsActive=true];
				        	System.debug(LoggingLevel.ERROR, 'DEBUGRF product: ' + product);
				        	if(product == null)
								ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'An error occurred while loading the related product'));
							else {
								PricebookEntry pbe = [SELECT Id FROM PricebookEntry WHERE Product2Id=:product.Id AND Pricebook2Id=:opportunity.Pricebook2Id];
					        	if(pbe == null) {
					            	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.CLI_ERR_Pricebook));
					        	} else {
					        		System.debug(LoggingLevel.ERROR, 'DEBUGRF quantity/unitprice: ' + li.oli.Quantity + '/' + li.oli.UnitPrice);
						       		
						       		if(li.initialSeries.equals('')) {
						       			li.oli.PricebookEntryId = pbe.Id;
						       			toInsert.add(li.oli);
						       		} else {
						       			OpportunityLineItem newOli = new OpportunityLineItem(
						       				//I nned to take all the info of the original line item
							       			OpportunityId = opportunity.Id,
							       			PricebookEntryId = pbe.Id,
							       			ServiceDate = li.oli.ServiceDate,
							       			Quantity = li.oli.Quantity,
							       			UnitPrice = li.oli.UnitPrice,
							       			Description = li.oli.Description,
							       			Category__c = li.oli.Category__c,
							       			Family__c = li.oli.Family__c,
							       			Series__c = li.oli.Series__c
							        	);
							       		     
						        		toDelete.add(li.oli);	
						        		toInsert.add(newOli);
						       		}
					        	}
							}
		        		}
		        		System.debug(LoggingLevel.ERROR, 'DEBUGRF li.oli: ' + li.oli);
	        		} else {
						System.debug(LoggingLevel.ERROR, 'DEBUGRF oli.Series__c: ' + li.oli.Series__c);
						Product2 product = [SELECT Id FROM Product2 WHERE Name=:li.oli.Series__c AND IsActive=true];
			        	
			        	if(product == null)
							ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'An error occurred while loading the related product'));
						else {
							PricebookEntry pbe = [SELECT Id FROM PricebookEntry WHERE Product2Id=:product.Id AND Pricebook2Id=:opportunity.Pricebook2Id];
				        	if(pbe == null) {
				            	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.CLI_ERR_Pricebook));
				        	} else {
					        	System.debug(LoggingLevel.ERROR, 'DEBUGRF quantity/unitprice: ' + li.oli.Quantity + '/' + li.oli.UnitPrice);
					       		
				        		li.oli.PricebookEntryId = pbe.Id;
				        		System.debug(LoggingLevel.ERROR, 'DEBUGRF li.oli: ' + li.oli);        		
				        		toInsert.add(li.oli);
				        	}
						}
					}
	    		}
        	}
			
			try {
	        	insert toInsert;
	        	update toUpdate;
	        	delete toDelete;
			} catch(Exception e) {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error during the save. Please check data inserted.'));
			}
        		
	        PageReference page = new PageReference('/' + opportunity.Id);
            page.setRedirect(true);
            return page;
        } catch(Exception e) {
        	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
        return null;
    }
    
    //Called when the 'Cancel' button is clicked from the Visualforce page
    public PageReference cancel() {
        try {
        	System.debug(LoggingLevel.ERROR, 'DEBUGRF cancel()');
	        PageReference page = new PageReference('/' + opportunity.Id);
            page.setRedirect(true);
            return page;
        } catch(Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
        return null;
    }
    
    public class LineItem {
    	public OpportunityLineItem oli {get;set;}
		public Decimal amount {get;set;}
		public Integer index {get;set;}
		public String initialSeries {get;set;}
    	
    	public LineItem(Id oppId, Integer i) {
    		oli = new OpportunityLineItem(
       			OpportunityId = oppId,
       			Quantity = 0,
       			UnitPrice = 0
        	);
        	amount = 0;
        	index = i;
        	initialSeries = '';
    	}
    	
    	public LineItem(OpportunityLineItem o, Integer i) {
    		oli = o;
        	amount = oli.TotalPrice;
        	initialSeries = oli.Series__c;
        	index = i;
    	}
    }
}