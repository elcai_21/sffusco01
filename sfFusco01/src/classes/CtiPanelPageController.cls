// Controller della pagina che contiene il pannello CTI per la comunicazione con Voiper CCS
// Before you can access external servers from an endpoint or redirect endpoint using Apex or any other feature,
// you must add the remote site to a list of authorized remote sites in the Salesforce user interface. 
// To do this, log in to Salesforce and select Your Name | Setup | Security Controls | Remote Site Settings.

global without sharing class CtiPanelPageController {
    
    //RF public list<selectoption> listaOperatoriAbilitati { get; set; }
    //public Id bollettaMailCampaignId { get; set; }
    //public String bollettaMailCampaignName { get; set; }
    //RF public String alertMessage { get; set; }
    //public List<Operatore_CTI__c> elencoOperatori;
    //RF public String operatoreSelezionato { get; set; }
    public String idOperatoreSelezionato { get; set; }
    public User selectedOperator { get; set; } //RF
    public Boolean profiloResponsabile { get; set; }
    public String currentProcess { get; set; }
    //RF public String spinCampaignLogin { get; set; }
    public Id campaignId { get; set; }
    public String campaignName { get; set; }
    public Id campaignMemberId { get; set; }
    //Riga20 pp1507 inserita variabile per distinzione di tipo campagna 
    public String campaignTipo { get; set; }
    public List<CTI_Operator__c> activeCampaigns { get; set; }
    //public String campaignSelected { get; set; }
    public String resultCode { get; set; }
    public Map<String, String> resultMap { get; set; }
    
    //FGalimberti
    /*RF public String NumberoTelefono {get;set;}
    public String forceCall {get;set;}*/
    
    public CtiPanelPageController() {
        //FGalimberti
        /*RFif(ApexPages.currentPage().getParameters().get('phone')!=null){
            NumberoTelefono=ApexPages.currentPage().getParameters().get('phone');
        }
        if(ApexPages.currentPage().getParameters().get('forceCall')!=null){
            forceCall=ApexPages.currentPage().getParameters().get('forceCall');
        }else{
            forceCall='0';
        }*/
        //Riga23 pp1507 valorizzazione iniziale variabile campaignTipo per //evitare di dare problemi con richiamai alla 
        //variabile quando è null
        //RF
        campaignName = '';
        //spinCampaignLogin = Constants_CCS__c.getInstance().Campaign_CTI__c;
        campaignTipo = 'start';
        //RF alertMessage = '';
        loadResultMap();
        try {
            selectedOperator = [SELECT Name, CTI_Agent_ID__c, CTI_Password__c, CTI_Phone_ID__c, CTI_Phone_Password__c
                                         FROM User WHERE Id=:Userinfo.getUserId() LIMIT 1]; //RF
            activeCampaigns = [SELECT Campaign__r.Name
            					FROM CTI_Operator__c WHERE Campaign__r.Stato_campagna_CTI__c='Attiva' AND User__c=:selectedOperator.Id];
            /*RF if (ApexPages.currentPage().getParameters().containskey('opId')) {
                this.idOperatoreSelezionato = Apexpages.currentPage().getParameters().get('opId');
                system.debug('@@@@@ idOperatoreSelezionato = ' + this.idOperatoreSelezionato);
            } else {
                User u = [Select Id, AccountID from User where Id = :Userinfo.getUserId() limit 1];
                String idProfiloOperatore = Constants_sistema__c.getInstance().ID_Profilo_Operatore_Teleselling_PRM__c;
                if (UserInfo.getProfileId().substring(0,15) == idProfiloOperatore) {
                    profiloResponsabile = false;
                } else {
                    profiloResponsabile = true;
                }
                this.listaOperatoriAbilitati = new List<Selectoption>();
                this.listaOperatoriAbilitati.add(new selectOption('',''));
                this.elencoOperatori = [Select Id, Name, Username_CTI__c, Account__c, Campagna__r.Id_campagna_CTI__c from Operatore_CTI__c where Username_CTI__c != null AND Account__c = :u.AccountID];
                if(!this.elencoOperatori.isEmpty()){
                    for(Operatore_CTI__c op : this.elencoOperatori){
                        listaOperatoriAbilitati.add(new SelectOption(op.Username_CTI__c, op.Name, false));
                    }
            }
            Constants_sistema__c cnst = Constants_sistema__c.getOrgDefaults();
            List<Campaign> cs = [Select Id, Name from Campaign 
                                 where Id != null and IsActive = true and Type = 'Bollett@Mail' and Status = 'In corso' and RecordTypeId = :cnst.RT_Campagna_Teleselling__c limit 1];
            if (!cs.isEmpty()) {
                this.bollettaMailCampaignId = cs[0].Id;
                this.bollettaMailCampaignName = cs[0].Name;
            }
            */
            /*RF if(ApexPages.currentPage().getParameters().containskey('cmId')) {
                this.campaignMemberId = Apexpages.currentPage().getParameters().get('cmId');
                //Riga 57 pp 1507 query modifica e aggiunto il campo type
                List<CampaignMember> cm = [Select Id, LeadId, CampaignId, Campaign.Name, Campaign.Type from CampaignMember where Id =: this.CampaignMemberId limit 1];
                if (!cm.isEmpty()) {
                    //RF this.CampaignId = cm[0].CampaignId;
                    //RF this.CampaignName = cm[0].Campaign.Name;
                    if(cm[0].Campaign.Type!=null && String.valueOf(cm[0].Campaign.Type).length()>0){ 
                        this.campaignTipo = cm[0].Campaign.Type;
                        system.debug('!!££$$ campaignTipo: ' + campaignTipo);
                    }
                }
            }
            if(ApexPages.currentPage().getParameters().containskey('process')) {
                this.currentProcess = Apexpages.currentPage().getParameters().get('process');
                system.debug('@@@@@ currentProcess = ' + this.currentProcess);
            }*/
        } catch (Exception ex) {
            system.debug('@@@@@ Eccezione: ' + ex.getMessage() + '\r\n@@@@@ Riga: ' + ex.getLineNumber());
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, 'Errore di sistema.\r\n' + ex.getMessage()));
        }
    }
    
    global static String getTimestamp() {
        DateTime currenttime = DateTime.now();
        return currenttime.format('yyyyMMddHHmmss', 'Europe/Rome');
    }
    
    //query Modulo info
    @RemoteAction
    global static List<List<SObject>> searchRecords(String phoneNumber) {
        system.debug('@@@ phone: ' + phoneNumber);
        
        String phone = '\'*' + phoneNumber.trim() + '\'';        
        List<List<SObject>> searchList = [FIND :phone IN PHONE FIELDS RETURNING Account(Id, Name, Owner.Name, Type), Contact(Id, Name, Owner.Name, Account.Name), Lead(Id, Name, Owner.Name, Status, Company)];
        //RF Inserire i campi in un Custom Setting?
        
        system.debug('@@@ searchList: ' + searchList);
        return searchList;
    }
    
    public List<SelectOption> getCampaignOptions() {
    	System.debug('@@@@@ getCampaignOptions');
    	
        List<SelectOption> options = new List<SelectOption>();
        if(activeCampaigns.isEmpty()) {
        	options.add(new SelectOption('', '-- Nessuna campagna attiva trovata --'));
        } else {
        	for(CTI_Operator__c CTIop : activeCampaigns) {
				options.add(new SelectOption(CTIop.Campaign__r.Name, CTIop.Campaign__r.Name)); 
			}
        }
        return options;
    }
    
    /*RF public PageReference changeCampaign() {
        System.debug('@@@@@ changeCampaign');
        
        for(CTI_Operator__c CTIop : activeCampaigns) { 
        	System.debug('@@@@@ CTIop.Campaign__r.Name = ' + CTIop.Campaign__r.Name);
            if(CTIop.Campaign__r.Name == campaignName) {
                campaignName = CTIop.Campaign__r.Name;
                //spinCampaignLogin = op.Campaign__r.Id_campagna_CTI__c;                     // progressive version
                //this.spinCampaignLogin = Constants_CCS__c.getInstance().campagnaCTI__c;       // blended version
            }
        }
        System.debug('@@@@@ spinCampaignLogin = ' + campaignName);
        return null;
    }*/
    
    public List<SelectOption> getResultOptions() {
    	System.debug('@@@@@ getResultOptions');
    	
        List<SelectOption> options = new List<SelectOption>();
    	for(String key : resultMap.keySet()) {
			options.add(new SelectOption(resultMap.get(key), key)); 
		}
        return options;
    }
    
    /*RF public PageReference changeResult() {
        System.debug('@@@@@ changeResult');
        System.debug('@@@@@ resultCode: ' + resultCode);
        return null;
    }*/
    
    public void loadResultMap() {
    	resultMap = new Map<String, String> {
	        'Numero telefonico non corrisponde a intestatario fornitura' => 'NTN',
			'Appuntamento telefonico' => 'APT',
			'Non interessato' => 'NOI',
			'Esito Positivo' => 'ESP',
			'Attiva servizio' => 'ASE',
			'Non rilascia consenso privacy' => 'NCP',
			'Rilascia consenso privacy' => 'RCP',
			'Non Interessato a rilasciare consenso' => 'NIC',
			'Disconosce offerta' => 'DIO',
			'Esercita ripensamento' => 'ERP',
			'Intestatario errato' => 'IER',
			'Termina Chiamata' => 'TCH',
			'Occupato' => 'OCC',
			'Numero errato/Rete occupata' => 'NER',
			'Cliente termina chiamata' => 'CTC',
			'Non risponde' => 'NRP',
			'Richiede altre prestazioni' => 'RAP',
			'Richiesta nuova attivazione EE' => 'RAE',
			'Richiesta cambio prodotto EE' => 'CPE',
			'Cliente moroso' => 'CMO',
			'Richiesta nuova attivazione GAS' => 'RAG',
			'Richiesta cambio prodotto GAS' => 'CPG',
			'Richiesta voltura GAS' => 'RVG',
			'Cliente non presente a portafoglio' => 'CNP',
			'REMI non offertabile' => 'RNO',
			'Non vuole cambiare fornitore' => 'NCN',
			'Richiesta nuova attivazione EE+GAS' => 'AEG',
			'Accetta offerta EE+GAS' => 'OEG',
			'Accetta offerta GAS' => 'AOG',
			'Accetta offerta EE' => 'AOE',
			'Richiede invio documentazione' => 'RDO',
			'Rifiuta registrazione' => 'RRE',
			'Fax/Segreteria' => 'FAX',
			'Deceduto' => 'RIP',
			'Titolare assente per lungo periodo' => 'LWH',
			'Numero inesistente' => 'NEN',
			'Non risponde, impossibile da contattare' => 'NAW',
			'Fornitura non più attiva' => 'NAF',
			'Irreperibile' => 'IRR',
			'Anziani/Categorie deboli' => 'OLD'
    	};
    }
    
    /*RF
    public PageReference logIn() {
        if (this.operatoreSelezionato == null) {
            system.debug('@@@@@ operatore non selezionato.');
            alertMessage = 'Selezionare un operatore.';
        } else {
            alertMessage = '';
            system.debug('@@@@@ operatore selezionato.');
        }
        return null;
    }
    
    //metodo che effettua la Callout per le richieste API verso CCS
    @RemoteAction
    global static String[] SendCall(String url, String chiamata) {
        system.debug('@@@@@ SEND CALL URL: ' + url + + ' ***Chiamata:'+ chiamata);
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
        req.setEndpoint(url);
        req.setMethod('GET');
        req.setTimeout(5000); // timeout in milliseconds
        try {
            if (!Test.IsRunningTest())
                res = http.send(req);
                // Log the XML content 
                System.debug(LoggingLevel.ERROR, '### CCS Response:'+res.getBody());
            if (res.getStatusCode() != 200) {
                String [] result = new String[] {'CALLOUT ERROR:status_code='+res.getStatusCode()+'status='+res.getStatus()+'body='+res.getBody(), chiamata };
                return result;
            }
            String [] result = new String[] { res.getBody(), chiamata };
            return result;
        } catch(System.CalloutException e) {
            String [] result = new String[] { 'CALLOUT EX ERROR:status_code='+res.getStatusCode()+'status='+res.getStatus()+'body='+res.getBody(), chiamata };
            return result;
        }
    }
        
    //update User CTI information
    @RemoteAction
    //global static void update_user_cti_info(Id id, String agent_username, String agent_password, String phone_username, String phone_password){
    global static void update_user_cti_info(String agent_password){
        try {
            User u = [SELECT Id, Password_CTI_corrente__c FROM User WHERE Id=:UserInfo.getUserId() LIMIT 1];
            //RF u.Password_CTI_corrente__c = agent_password;
            system.debug('@@@@@ update CTI user information:\r\nPassword_CTI_corrente__c = ' + agent_password);
            update(u);
        } catch (Exception ex) {
            system.debug('@@@@@ Error: ' + ex.getMessage());
            String body = 'Eccezione>>' +ex.getMessage()+'  Stacktrace>>>'+ex.getStackTraceString();
            //RF Utils.sendMail('','Debug notifier','noreplay@A2A.it','A2A ['+System.UserInfo.getOrganizationId()+'] CtiPanel - insert task esito',Body);
        }
    }

    global static String[] insert_task_esito(String objectId, String agent_name, String idOperatore, String phone_username, String Transaction_id, String campaignName, String campaignId, String causale_esito, String esito_contatto, String description) {
        try {
            Task task = new Task();
            RF task.RecordTypeId = Constants_sistema__c.getInstance().RT_Esiti_contatto__c;
            task.Subject = 'Esito contatto';
            task.Status = 'Completata';
            task.OwnerId = UserInfo.getUserId();
            
            system.debug('@@@ objectId: '+objectId);
            if (objectId.SubString(0,3) == 'a08') {
                task.WhatId = objectId;
            } else if (objectId.SubString(0,3) == '00Q') {
                task.WhoId = objectId;
                if (campaignId != '' || campaignId != null) {
                    task.Nome_Campagna__c = campaignName;
                    task.Codice_campagna__c = campaignId;
                }
            }
            task.Nome_Operatore_TLS__c = agent_name;            //Cognome nome operatore
            task.Operatore_CTI__c = idOperatore;                //username Agent CTI //agent_username_cookie
            task.Numero_postazione__c = phone_username;
            task.Transaction_id_vo__c = Transaction_id;         //utilizzato per l'associazione con il nomeFile
            task.Causale_esito__c = causale_esito;
            task.Esito_contatto__c = esito_contatto;
            task.Description = description;
            insert task;
            String [] result = new String[] { task.Id, Causale_esito };
            return result;
        } catch (Exception ex) {
            system.debug('@@@@@ Error: ' + ex.getMessage());
            String body = 'Eccezione>>' +ex.getMessage()+'  Stacktrace>>>'+ex.getStackTraceString()+'\r\n\r\n Task:'+ task.Id + ' \r\n\r\n ObjectId:'+objectId;
            body += '\n\n'+objectId+','+agent_name+','+idOperatore+','+phone_username+','+Transaction_id+','+campaignName+','+campaignId+','+causale_esito+','+esito_contatto+','+description;
            Utils.sendMail('','Debug notifier','noreplay@A2A.it','A2A ['+System.UserInfo.getOrganizationId()+'] CtiPanel - insert task esito',Body);
            return null;
        }
    }*/
    

    /*RF@RemoteAction
    global static String[] hangup_status(String urlHangup, String urlStatus, String[] taskParam) {
        String[] response1;
        String[] response2;
        try {
            system.debug('@@@@@ urlHangup = ' + urlHangup);
            system.debug('@@@@@ urlStatus = ' + urlStatus);
            system.debug('@@@@@ taskParam = ' + taskParam);
            response1 = SendCall(urlHangup, 'HANGUP');
            List<String> responseV = response1[0].split(':');
            if (responseV[0] == 'SUCCESS') {
                system.debug('@@@@@ response SendCall(HangUp) = SUCCESS');
                response1 = SendCall(urlStatus, 'STATUS');
                responseV = response1[0].split(':');
                if (responseV[0] == 'SUCCESS') {
                    system.debug('@@@@@ response SendCall(Status) = SUCCESS');
                    if (taskParam != null && taskParam.size() != 0) {
                        //RF response2 = insert_task_esito(taskParam[0], taskParam[1], taskParam[2], taskParam[3], taskParam[4], taskParam[5], taskParam[6], taskParam[7], taskParam[8], taskParam[9]);
                        return response2;
                    } else {
                        return response1;
                    }
                } else {
                    system.debug('@@@@@ response SendCall(HangUp) = FAIL');
                    return null;
                }
            } else {
                system.debug('@@@@@ response SendCall(HangUp) = FAIL');
                return null;
            }
            return response2;
        } catch (Exception ex) {
            system.debug('@@@@@ Error: ' + ex.getMessage());
            String body = 'Eccezione>>' +ex.getMessage()+'  Stacktrace>>>'+ex.getStackTraceString()+'\r\n\r\n Response1:'+ response1 + ' \r\n\r\n Response2:' + response2;
            //RF Utils.sendMail('','Debug notifier','noreplay@A2A.it','A2A ['+System.UserInfo.getOrganizationId()+'] CtiPanel - hangup status',Body);
            return null;
        }
    }
    
    //FGALIMBERTI 20131003 Aggiornamento
    @RemoteAction
    global static String[] hangup_update_status(String urlHangup, String urlStatus, String urlUpdate) {
        String[] response1;
        String[] response2;
        
        try {
            //RF EAIAutoletturaSFA.EAIFrontendOutboundInterfaceEndpoint EAI = new EAIAutoletturaSFA.EAIFrontendOutboundInterfaceEndpoint();
            //RF AutoletturaSFA.Esito_element EAIresp = new AutoletturaSFA.Esito_element();
            system.debug('@@@@@ urlHangup = ' + urlHangup);
            system.debug('@@@@@ urlStatus = ' + urlStatus);
            system.debug('@@@@@ urlUpdate = ' + urlUpdate); 
            response1 = SendCall(urlHangup, 'HANGUP');
            List<String> responseV = response1[0].split(':');
            if (responseV[0] == 'SUCCESS') {
                system.debug('@@@@@ response SendCall(HangUp) = SUCCESS');
                
                system.debug('!!££$$ prima di spleep');
                
                //RF EAIresp = EAI.Sleep(2);
                    
                response1 = SendCall(urlUpdate, 'UPDATE LEAD');
                responseV = response1[0].split(':');
                
                if (responseV[0] == 'SUCCESS') {
                    system.debug('@@@@@ response SendCall(Update) = SUCCESS');
                    //FGALIMBERTI 20131003 Cerco di perdere tempo
                    //RF EAIresp = EAI.Sleep(2);
                    
                    response1 = SendCall(urlStatus, 'STATUS');
                    responseV = response1[0].split(':');
                    if (responseV[0] == 'SUCCESS') {
                        system.debug('!!@@@@@ response SendCall(Status) = SUCCESS');
                        return response1;
                    }
                } else {
                    system.debug('@@@@@ response SendCall(HangUp) = FAIL');
                    return null;
                }
            } else {
                system.debug('@@@@@ response SendCall(HangUp) = FAIL');
                return null;
            }
            return response2;
        } catch (Exception ex) {
            system.debug('@@@@@ Error: ' + ex.getMessage());
            String body = 'Eccezione >> ' + ex.getMessage() + '  Stacktrace >> ' + ex.getStackTraceString() + '\r\n\r\n Response1: ' + response1 + ' \r\n\r\n Response2: ' + response2;
            //RF Utils.sendMail('', 'Debug notifier', 'noreplay@A2A.it', 'A2A ['+System.UserInfo.getOrganizationId()+'] CtiPanel - hangup status', Body);
            return null;
        }
    }
    
    //query Modulo info
    @RemoteAction
    global static String[] query_modulo_info(Id id) {
        system.debug('@@@ moduloId: '+id);
        Modulo__c m = [SELECT Id, Canale__c FROM Modulo__c WHERE Id = :id LIMIT 1];
        System.debug('#####Canale__c: ' + m.Canale__c);
        String [] modulo = new String[] {Id, m.Canale__c};
        return(modulo);
    }
    
    //query Lead SPIN-EAI-SFA
    @RemoteAction
    global static String[] query_lead(String phone) {
        System.debug('#####Phone: ' + phone);
        Lead l = [SELECT Id, Caller_Id__c, Codice_Promozione_TLS__c FROM Lead WHERE Caller_Id__c = :phone LIMIT 1];
        String [] lead = new String[] {l.Id, l.Caller_Id__c, l.Codice_Promozione_TLS__c};
        return(lead);
    }*/
    
    /*
    @RemoteAction
    global static String[] query_lead(String phone) {
        System.debug('#####Caller_Id__c: ' + phone);
        CampaignMember l = [SELECT Caller_Id__c,CampaignId,Codice_promozione_TLS__c,LeadId FROM CampaignMember WHERE Caller_Id__c = :phone LIMIT 1];
        System.debug('#####Codice_Promozione_TLS__c sul lead: ' + l.Codice_Promozione_TLS__c);
        String [] lead = new String[] {l.Id, l.Caller_Id__c, l.Codice_Promozione_TLS__c};
        return(lead);
    }
    */
}