public with sharing class QualityCheckTelesellerController {

    public Flow.Interview.QualityCheck_Telefonico_TLS_Domestici QualityCheckTLSFlow {get; set;}
    
    //public Modulo__c modulo {get;Set;}
    public String idModulo {get;set;}
    public String NumeroTelefono {get;set;}
    
    public QualityCheckTelesellerController() {
        if (ApexPages.currentPage().getParameters().containskey('mId') && 
            ApexPages.currentPage().getParameters().get('mId') != null &&
            ApexPages.currentPage().getParameters().get('mId') != '')
        {
            this.NumeroTelefono = ApexPages.currentPage().getParameters().get('phone');
            this.idModulo = ApexPages.currentPage().getParameters().get('mId');
            /*list<Modulo__c> L_modulo = [select Id, Data_soglia_ricontatto__c from modulo__c where id = :idModulo limit 1];
            if(!L_modulo.isempty()){
                this.modulo = L_modulo.get(0);
            } else {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Problemi nel recupero dell\'offerta.'));
            }*/
        } else {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Mancano dei parametri necessari al funzionamento della pagina.'));
        }
    }
    
    public String getModuloID() {
        if (QualityCheckTLSFlow == null) return '';
        else return QualityCheckTLSFlow.IdModulo;
    }
    
    public String getFinishPage(){
        String opId = '';
        String opNum = '';
        if (QualityCheckTLSFlow != null) {
            opId = QualityCheckTLSFlow.QCT_OperatoreId;
            opNum = QualityCheckTLSFlow.QCT_OperatoreNumero;
        }
        String url = '/apex/ModuliQualityCheck?opId=' + opId + '&opNum=' + opNum;
        //PageReference page = new PageReference(url);
        //page.setRedirect(true);
        //return page;
        return url;
    }
}