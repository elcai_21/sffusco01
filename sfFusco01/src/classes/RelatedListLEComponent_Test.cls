/**
 * Created by g.rota on 28/04/2017.
 */

@IsTest
private class RelatedListLEComponent_Test {
    static testMethod void testGetAvailableRecordTypes() {
        RelatedListLEComponent.getAvailableRecordTypes('Opportunity');
    }

    static testMethod void testGetSessionIdAndInitPushTopic() {
        RelatedListLEComponent.getSessionIdAndInitPushTopic('Opportunity','StageName','AccountId','000000000000000','TestPT');
        List<PushTopic> pushTopics = [SELECT Id,Name FROM PushTopic WHERE Name='TestPT'];
        System.assertEquals(1,pushTopics.size());
    }

    static testMethod void testDeleteRecordById() {
    	Account a = new Account(Name='Tizio');
    	insert a;
        RelatedListLEComponent.deleteRecordById(a.Id);
        List<Account> accs = [SELECT Id,Name FROM Account];
        System.assertEquals(0,accs.size());
    }

    static testMethod void testGetNamesFromId() {
    	Account a = new Account(Name='Tizio');
    	insert a;
    	Opportunity o = new Opportunity(Name='OppTest',StageName='New',CloseDate=date.today(),AccountId=a.Id) ;
    	insert o;
    	List<String> param = new List<String>{'0,AccountId,'+a.Id,'1,AccountId,'+a.Id};
        List<String> ret = RelatedListLEComponent.getNamesFromId(param);
        System.assertEquals('Tizio',ret[0].split(',')[2]);
    }

    static testMethod void testGetIcon() {
        String ret1 = RelatedListLEComponent.getIcon('Opportunity');
        System.assertEquals('standard:opportunity',ret1);
        String ret2 = RelatedListLEComponent.getIcon('Uvuwewe');
        System.assertEquals(null,ret2);
    }

    static testMethod void testGetPermissionsAndErrors() {
        Map<String,Object> ret1 = RelatedListLEComponent.getPermissionsAndErrors('Opportunity','StaZgeName','AccountId');
        System.assertEquals(false,ret1.get('CampiENomeOggValidi'));
        Map<String,Object> ret2 = RelatedListLEComponent.getPermissionsAndErrors('Opportunity','StageName,Campaign.Chapter__r.Name','AccountId');
        System.debug(ret2);
        System.assertEquals(true,ret2.get('CampiENomeOggValidi'));
        System.assertNotEquals(null,ret2.get('Campi'));
        System.assertNotEquals(null,ret2.get('Oggetto'));
        List<String> listaLabel = (List<String>) ret2.get('Label');
        System.assertEquals(2,listaLabel.size());
        List<String> listaTipi = (List<String>) ret2.get('Tipi');
        System.assertEquals('PICKLIST',listaTipi[0]);
    }

    static testMethod void testGetPluralLabelAndRelationshipName() {
    	Account a = new Account(Name='Tizio');
    	insert a;
        List<String> ret = RelatedListLEComponent.getPluralLabelAndRelationshipName('Opportunity','AccountId',a.Id);
        System.assertNotEquals(null,ret[0]);
        System.assertEquals('Opportunities',ret[1]);
        List<String> ret2 = RelatedListLEComponent.getPluralLabelAndRelationshipName('Opportunity','AccZountId',a.Id);
        System.assertEquals(null,ret2);
    }

    static testMethod void testGetRelatedSortedObjects() {
    	User u = [SELECT Id FROM User LIMIT 1];
    	Account a = new Account(Name='Tizio');
    	insert a;
    	List<Opportunity> opps = new List<Opportunity>();
    	Opportunity o = new Opportunity(Name='OppTest',StageName='New',CloseDate=date.today(),AccountId=a.Id,OwnerId=u.Id);
		Opportunity o2 = new Opportunity(Name='OppTest2',StageName='New',CloseDate=date.today().addDays(1),AccountId=a.Id,OwnerId=u.Id);
		opps.add(o);
		opps.add(o2);
    	insert opps;
    	UserRecordAccess usa = [SELECT RecordId,HasReadAccess,HasEditAccess,HasDeleteAccess FROM UserRecordAccess WHERE UserId=:u.Id AND RecordId =:o.Id];
    	UserRecordAccess usa2 = [SELECT RecordId,HasReadAccess,HasEditAccess,HasDeleteAccess FROM UserRecordAccess WHERE UserId=:u.Id AND RecordId =:o2.Id];
    	Test.startTest();
    	List<List<SObject>> ret = null;
    	System.runAs(u)
    	{
        	ret = RelatedListLEComponent.getRelatedSortedObjects('Opportunity','StageName,CloseDate','AccountId',a.Id,'CloseDate','DESC',5);
    	}
        Test.stopTest();
        Integer nRecordAtteso = (usa.HasReadAccess && usa2.HasReadAccess) ? 2 : ((usa.HasReadAccess || usa2.HasReadAccess)) ? 1 : 0;
        System.assertEquals(nRecordAtteso,ret[0].size());
        if (nRecordAtteso==2)
        {
        	System.assertEquals('OppTest2',ret[0][0].get('Name'));
        }
    }

    static testMethod void testGetRelatedObjects() {
    	User u = [SELECT Id FROM User LIMIT 1];
    	Account a = new Account(Name='Tizio');
    	insert a;
    	List<Opportunity> opps = new List<Opportunity>();
    	Opportunity o = new Opportunity(Name='OppTest',StageName='New',CloseDate=date.today(),AccountId=a.Id,OwnerId=u.Id);
		Opportunity o2 = new Opportunity(Name='OppTest2',StageName='New',CloseDate=date.today().addDays(1),AccountId=a.Id,OwnerId=u.Id);
		opps.add(o);
		opps.add(o2);
    	insert opps;
    	
    	UserRecordAccess usa = [SELECT RecordId,HasReadAccess,HasEditAccess,HasDeleteAccess FROM UserRecordAccess WHERE UserId=:u.Id AND RecordId =:o.Id];
    	UserRecordAccess usa2 = [SELECT RecordId,HasReadAccess,HasEditAccess,HasDeleteAccess FROM UserRecordAccess WHERE UserId=:u.Id AND RecordId =:o2.Id];
    	Test.startTest();
    	List<Object> ret = null;
    	System.runAs(u)
    	{
        	ret = RelatedListLEComponent.getRelatedObjects('Opportunity','StageName,CloseDate','AccountId',a.Id,5);
    	}
        Test.stopTest();
        Integer nRecordAtteso = (usa.HasReadAccess && usa2.HasReadAccess) ? 2 : (usa.HasReadAccess || usa2.HasReadAccess) ? 1 : 0;
        List<SObject> listaRecord = (List<SObject>)ret[0];
        System.assertEquals(nRecordAtteso,listaRecord.size());
    }
}