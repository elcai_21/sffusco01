public class OutBoundServicesVoiperCCS {

    //>MP 29/04/2013 - Integrazione CCS    
    //Callout richiesta generazione CSV lista campagna Features
    /*RF @future (callout=true)
    public static void callOutGeneraListaFuture(list<string> log,String idCampagna, Map<String, String> param){
        //system.debug('## callOutGeneraListaFuture');
        OutBoundServicesVoiperCCS.addDebug(log, 'callOutGeneraListaFuture');
        callOutGeneraLista(log,idCampagna, param);
    }
    
    //Callout elaborazione CSV esiti Features
    @future (callout=true)
    public static void callOutElaboraEsitiFuture(list<string> log,String idCampagna, Map<String, String> param){
        //system.debug('## callOutElaboraEsitiFuture');
        OutBoundServicesVoiperCCS.addDebug(log, 'callOutElaboraEsitiFuture');
        callOutElaboraEsiti(log,idCampagna, param);
    }*/
    
    //Callout richiesta generazione CSV lista campagna
    public static String callOutGeneraLista(list<String> log,String idCampagna, Map<String, String> paramCallout) {     	
    	OutBoundServicesVoiperCCS.addDebug(log, 'callOutGeneraLista');
    	
    	try {
    		String esitoChiamata = '';
    		String xml = OutBoundServicesVoiperCCS.getXMLIntegrazioneCCS(log, 'send', idCampagna, paramCallout);
			OutBoundServicesVoiperCCS.addDebug(log, '### xml: ' + xml);
			
			System.debug('##Elimino i caratteri speciali');
			xml = normalizzaXML(xml);
			
			//chiamo classe per invio callout            
            if(!Test.isRunningTest()){
				OutBoundServicesVoiperCCS.addDebug(log, '###Chiamo la classe di invio callout');
	            WS_CCSIntegration.CCSIntegrationSoap solsoap = new WS_CCSIntegration.CCSIntegrationSoap();
	            //effettuo la chiamata
            	esitoChiamata = solsoap.sendRequest(xml, ''); // invoke CallOut
	            OutBoundServicesVoiperCCS.addDebug(log, '###esitoChiamata >> ' + esitoChiamata);
	            if(esitoChiamata != 'OK') {
                    return 'ERROR';	//RF
	            	//RF throw new WrtsException('Callout in errore: ' + esitoChiamata);	                           
	            } else { 
	                OutBoundServicesVoiperCCS.addDebug(log, 'OK. Callout effettuata con successo');
	        		return 'OK. Callout effettuata con successo';
	            }
            } else {
        		OutBoundServicesVoiperCCS.addDebug(log, '### Chiamata proveniente da un metodo di TEST - salto invio callout');
        		return 'TEST';
        	}	
    	} catch(Exception ex) {
    		  /*RF Utils.sendMail(Constants_sistema__c.getInstance().Email_Supporto__c, 'Salesforce Support for A2A', 'noreply@a2a.it', 
                'A2A ['+System.UserInfo.getOrganizationId()+'] - Errore inoltro richiesta generazione lista per campagna: ' + idCampagna,
                'A2A ['+System.UserInfo.getOrganizationId()+'] - L\'invio della callout per la generazione lista è stata respinta.\nMessaggio eccezione: ' + ex.getmessage());*/
    		return 'Error. Dettagli: '+ ex.getmessage();
    	}
    }
    
    //Callout elaborazione CSV esiti
    /*RF
    public static void callOutElaboraEsiti(list<String> log,String idCampagna, Map<String, String> paramCallout){     	
    	try{
    		String esitoChiamata = '';
    		String xml = OutBoundServicesVoiperCCS.getXMLIntegrazioneCCS(log,'receive', idCampagna, paramCallout);
			OutBoundServicesVoiperCCS.addDebug(log,'### xml:' + xml);
			
			OutBoundServicesVoiperCCS.addDebug(log,'##Elimino i caratteri speciali');
			xml = OutBoundDynamicServices.normalizzaXML(xml);
			
			//chiamo classe per invio callout            
            if(!Test.isRunningTest()){
				OutBoundServicesVoiperCCS.addDebug(log,'###Chiamo la classe di invio callout'); 
	            WS_CCSIntegration.CCSIntegrationSoap solsoap  = new WS_CCSIntegration.CCSIntegrationSoap();	                    
	            //effettuo la chiamata
            	esitoChiamata = solsoap.receiveRequest(xml, ''); // invoke CallOut 	                       
	            OutBoundServicesVoiperCCS.addDebug(log,'###esitoChiamata>>'+esitoChiamata);
	            if (esitoChiamata != 'OK') {
	            	throw new WrtsException('Callout in errore: '+ esitoChiamata);                           
	            } else { 
	                OutBoundServicesVoiperCCS.addDebug(log,'Callout effettuata con successo');	                             
	            } 
            }
            else
        	{
        		OutBoundServicesVoiperCCS.addDebug(log,'### Chiamata proveniente da un metodo di TEST - salto invio callout');
        	}		
    		
    	}catch(Exception ex){
    		  Utils.sendMail(Constants_sistema__c.getInstance().Email_Supporto__c, 'Salesforce Support for A2A', 'noreply@a2a.it', 
                'A2A ['+System.UserInfo.getOrganizationId()+'] - Errore inoltro richiesta generazione lista per campagna: ' + idCampagna,
                'A2A ['+System.UserInfo.getOrganizationId()+'] - L\'invio della callout per la generazione lista è stata respinta.\nMessaggio eccezione: ' + ex.getmessage());
    	}
    }*/
    
    public static String getXMLIntegrazioneCCS(List<String> log, String operation, String idCampagna, Map<String, String> param) {    	
    	List<Campaign> campaign = [SELECT Id, Id_campagna_CTI__c, Id_lista_CTI__c, stato_campagna_cti__c, Name, stato_lista_cti__c,
                                   ParentId, Parent.id_campagna_CTI__c, Template_cti__c, Parent.Name, Stato_invio_lista__c
                                   FROM Campaign WHERE Id = :idCampagna];
    	
    	OutBoundServicesVoiperCCS.addDebug(log, '##Parent Id: ' + campaign.get(0).ParentId);
				
		String xml = '<request>';
		xml += '<process>' + operation + '</process>';
		xml += '<list><id>' +  campaign.get(0).Id + '</id><name>' +  campaign.get(0).Name + '</name><idCTI>' +  campaign.get(0).Id_lista_CTI__c + '</idCTI></list>';
		
		if(campaign.get(0).ParentId != null){
			//Campaign parentCampaign = [select id, Name from Campaign where id=: campaign.get(0).ParentId];		   		
			xml += '<campaign><id>' + campaign.get(0).ParentId + '</id><name>' + campaign.get(0).Parent.Name + '</name><idCTI>' + campaign.get(0).Parent.id_campagna_CTI__c + '</idCTI></campaign>';
		}
		
		if(param.get('uri') != '' && param.get('uri') != null) {
    		xml += '<uri>' + param.get('uri') + '</uri>'; 
		}
		
		xml += '<UserInfo>';
        xml += '<userId>' + System.Userinfo.getUserId() + '</userId>';
        xml += '<username>' + System.Userinfo.getUserName() + '</username>';
        xml += '</UserInfo>';
    	xml += '</request>';
    	
    	return xml;    	
    }
    
    public static String normalizzaXML(String input) {
        system.debug('normalizzaXML');
        //system.debug('input>>'+input);
        String output = '';
        if(input == null || input == '') {
            return input;
        }

        //output = input.replaceall('&','&amp;');
		// 2012-11-22 PES (Fix per "LimitException: Regex too complicated")
		for(Integer i=0; i<input.length(); i+=30000) {
			String chunk = input.substring(i, Math.min(i+30000, input.length()));
			//output += chunk.replaceall('&','&amp;');
			output += chunk.replaceall('&','&amp;').replaceall('<br>', '<br/>'); //M.P. Fix per campi formula che contengono carattere di a capo
		}
        //system.debug('output>>'+output);
        
        return output;
    }
    
    
    public static void addDebug(list<String> log, String testo){
        system.debug(testo);
        log.add('['+datetime.now().format()+']  '+ testo);
    }  
    //<MP 29/04/2013 - Integrazione CCS
}