@isTest
global class testCallouts implements WebServiceMock {
    global void doInvoke(
        Object stub,
        Object request,
        Map<String, Object> response,
        String endpoint,
        String soapAction,
        String requestName,
        String responseNS,
        String responseName,
        String responseType) {
            Profile.revokePermissionResponse_element respElement = new Profile.revokePermissionResponse_element();
            respElement.revokePermissionResult = true;
            response.put('response_x', respElement); 
        }
}