global without sharing class ContactUtil {

    webService static String deleteContact(Id id) { 
        Contact c = new Contact(id = id);
        try {
            UserRecordAccess ura = [select RecordId, HasEditAccess from UserRecordAccess where UserId = :UserInfo.getUserId() and RecordId = :id];
            if(ura.HasEditAccess) 
                delete c;
            else 
                throw new deleteException('You do not have the level of access necessary to perform the operation you requested. Please contact the owner of the record or your administrator if access is necessary.');
            return '';
        } catch (Exception e) {
            return e.getMessage();
        }
    }
    
    @isTest
    static void testDeleteContact() {
        Account a = new Account(Name = 'Test');
        insert a;
        Contact c = new Contact(LastName = 'Test', Account = a);
        insert c;
        deleteContact(c.Id);
        
        //Make sure the contact was deleted
        Contact[] testContact = [select Id from Contact where Id = :c.Id];
        System.assert(testContact.isEmpty());
    }
    
    public class deleteException extends Exception {}
}