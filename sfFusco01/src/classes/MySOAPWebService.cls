global class MySOAPWebService {

     global class Cliente {
          webservice String Id;
          webservice String accountId;
          webservice String productId;
          webservice String orderType;
          webservice String orderStatus;
          webservice String billingType;
          webservice String closedById;
     }
     
     global class Risposta {
          webservice String codice;
          webservice  String descrizione;
     }

     webservice static List<Risposta> getWorkOrder(Risposta rs, List<Cliente> clienti) {
          Risposta r = new Risposta();
          Risposta r2 = new Risposta();
          List<Risposta> lista = new List<Risposta>();
          lista.add(r);
          lista.add(r2);
          return lista;
     }
}