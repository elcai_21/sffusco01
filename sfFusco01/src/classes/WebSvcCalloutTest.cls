@isTest
private class WebSvcCalloutTest {
    @isTest static void testEchoString() {              
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new testCallouts());
        
        Profile.EvalancheWebserviceProfile2Port sample = new Profile.EvalancheWebserviceProfile2Port();
        sample.endpoint_x = 'http://api.salesforce.com/foo/bar';
        
        // This invokes the EchoString method in the generated class
        Boolean output = sample.revokePermission(1);
        
        // Verify that a fake result is returned
        System.assertEquals(true, output); 
    }
}