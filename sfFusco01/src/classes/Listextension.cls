global class Listextension {
    
    public Account account {get;set;}
    public List<Account> lista {get;set;}
    
    public Listextension(ApexPages.StandardController controller) {
        this.account = (Account)controller.getRecord();
        
        lista = [SELECT Id, Name, Owner.Name FROM Account LIMIT 30];
    }
}