@RestResource(urlMapping='/Test')
global class TestWS {

    @HttpGet
    global static ResponseClass doGet() {
        ResponseClass rc = new ResponseClass('1000', 'Prova poba');
        return rc;
    }

    global class ResponseClass {
    
        global String code {get; set;}
        global String description {get; set;}
        
        public ResponseClass(String code, String description) {
            this.code = code;
            this.description = description;
        }       
    }
}