/**
 * Created by g.rota on 10/04/2017.
 */

public with sharing class RelatedListLEComponent {

    private without sharing class SystemMode {
        private String getIconSM(String nomeOggetto)
        {
            List<Schema.DescribeTabSetResult> tabSetDesc = Schema.describeTabs();
            for(Schema.DescribeTabSetResult tsr : tabSetDesc) {
                List<Schema.DescribeTabResult> dtrs = tsr.getTabs();
                for (Schema.DescribeTabResult dtr:dtrs)
                {
                    if (dtr.getSobjectName()==nomeOggetto)
                    {
                        String url = dtr.getIcons()[2].getUrl();
                        url = url.substringAfter('/t4v35/');
                        String category = url.substringBefore('/');
                        String iconName = url.substringBetween('/','.svg');
                        return category+':'+iconName;
                    }
                }
            }
            return null;
        }
        private String getSessionIdAndInitPushTopicSM(String nomeOgg,String stringaCampi,String nomeCampoRelazione,String recordId,String nomePushTopic)
        {
            List<PushTopic> pushTopics = [SELECT Id,Name FROM PushTopic WHERE Name=:nomePushTopic];
            if (null == pushTopics || pushTopics.size() == 0) {
                System.debug('Creo');
                String query = 'SELECT Id';
                String[] campi = stringaCampi.split(',');
                for (String campo:campi)
                {
                    if (campo!='Id')
                        query+= ','+campo;
                }
                query+= ' FROM '+nomeOgg+' WHERE '+nomeCampoRelazione+'=\''+recordId+'\'';
                PushTopic topic = new PushTopic();
                topic.Name = nomePushTopic;
                topic.Query = query;
                topic.NotifyForFields = 'Select';
                topic.NotifyForOperationCreate = true;
                topic.NotifyForOperationUpdate = true;
                topic.NotifyForOperationUndelete = true;
                topic.NotifyForOperationDelete = true;
                topic.ApiVersion = 37.0;
                insert topic;
            }
            else System.debug('Cè già');
            return UserInfo.getSessionId();
        }
    }

    @AuraEnabled
    public static List<Object> getRelatedObjects(String nomeOgg,String stringaCampi,String nomeCampoRelazione,String recordId,Integer limite)
    {
        String[] campi = stringaCampi.split(',');
        String query = 'SELECT Id';
        for (String campo:campi)
        {
            if (campo!='Id')
                query+= ','+campo;
        }
        query+= ' FROM '+nomeOgg+' WHERE '+nomeCampoRelazione+'=\''+recordId+'\'';
        query+= ' ORDER BY CreatedDate DESC';
        query+= ' LIMIT 100';
        System.debug(query);
        List<SObject> listaFull = Database.query(query);
        List<SObject> lista = new List<SObject>();
        for (Integer i=0;i<listaFull.size();i++)
            if (i<limite)
                lista.add(listaFull[i]);
        /*
        List<Id> recordIds = new List<Id>();
        for (SObject o:lista) recordIds.add( (Id)o.get('Id') );
        List<SObject> permissionList = [SELECT RecordId,HasReadAccess,HasEditAccess,HasDeleteAccess FROM UserRecordAccess 
        WHERE UserId=:UserInfo.getUserId() AND RecordId IN :recordIds];
        */
        List<SObject> permissionList = new List<SObject>();
        for (SObject o:lista) //QUERY NEL FOR :D
        {
            Id i = (Id)o.get('Id');
            SObject permission = [SELECT RecordId,HasReadAccess,HasEditAccess,HasDeleteAccess FROM UserRecordAccess WHERE UserId=:UserInfo.getUserId() AND RecordId =:i];
            permissionList.add(permission);
        }

        List<SObject> orderedPermissionList = new List<SObject>();
        List<Integer> recordSenzaRead = new List<Integer>();
        for (Integer i=0;i<lista.size();i++)
        {
            for (Integer j=0;j<permissionList.size();j++)
            {
                if (permissionList[j].get('RecordId')==lista[i].get('Id'))
                {
                    //segno record senza Read access
                    if (!(Boolean)permissionList[j].get('HasReadAccess'))
                    {
                        recordSenzaRead.add(i);
                    }
                    orderedPermissionList.add(permissionList[j]);
                    break;
                }
            }
        }
        //rimuovo record senza Read access
        for (Integer i=(recordSenzaRead.size()-1);i>=0;i--)
        {
            lista.remove(i);
            orderedPermissionList.remove(i);
        }
        List<Object> ret = new List<Object>();
        ret.add(lista);
        ret.add(orderedPermissionList);
        ret.add(listaFull.size());
        return ret;
    }

    @AuraEnabled
    public static List<List<SObject>> getRelatedSortedObjects(String nomeOgg,String stringaCampi,String nomeCampoRelazione,
        String recordId,String campoDaOrdinare,String ordine,Integer limite)
    {
        String query = 'SELECT Id,Name';
        String[] campi = stringaCampi.split(',');
        for (String campo:campi)
        {
            if (campo!='Id' && campo!='Name')
                query+= ','+campo;
        }
        query+= ' FROM '+nomeOgg+' WHERE '+nomeCampoRelazione+'=\''+recordId+'\'';
        query+= ' ORDER BY '+campoDaOrdinare+' '+ordine+' NULLS LAST';
        query+= ' LIMIT '+limite;
        List<SObject> lista = Database.query(query);
        /* A MALI ESTREMI...
        List<Id> recordIds = new List<Id>();
        for (SObject o:lista) recordIds.add( (Id)o.get('Id') );
        List<SObject> permissionList = [SELECT RecordId,HasReadAccess,HasEditAccess,HasDeleteAccess FROM UserRecordAccess 
        WHERE UserId=:UserInfo.getUserId() AND RecordId IN :recordIds];
        */
        // ESTREMI RIMEDI
        List<SObject> permissionList = new List<SObject>();
        for (SObject o:lista)
        {
            Id i = (Id)o.get('Id');
            SObject permission = [SELECT RecordId,HasReadAccess,HasEditAccess,HasDeleteAccess FROM UserRecordAccess WHERE UserId=:UserInfo.getUserId() AND RecordId =:i];
            permissionList.add(permission);
        }
        List<SObject> orderedPermissionList = new List<SObject>();
        List<Integer> recordSenzaRead = new List<Integer>();
        for (Integer i=0;i<lista.size();i++)
        {
            for (Integer j=0;j<permissionList.size();j++)
            {
                if (permissionList[j].get('RecordId')==lista[i].get('Id'))
                {
                    //segno record senza Read access
                    if (!(Boolean)permissionList[j].get('HasReadAccess'))
                    {
                        recordSenzaRead.add(i);
                    }
                    orderedPermissionList.add(permissionList[j]);
                    break;
                }
            }
        }
        //rimuovo record senza Read access
        if (recordSenzaRead.size()>0)
        {
            for (Integer i=(recordSenzaRead.size()-1);i>=0;i--)
            {
                lista.remove(i);
                orderedPermissionList.remove(i);
            }
        }
        List<List<SObject>> ret = new List<List<SObject>>();
        ret.add(lista);
        ret.add(permissionList);
        return ret;
    }

    @AuraEnabled
    public static List<String> getPluralLabelAndRelationshipName(String nomeOgg,String nomeCampoRelazione,Id recordId)
    {
        List<String> ret = new List<String>();
        Schema.DescribeSobjectResult[] results = Schema.describeSObjects(new List<String>{nomeOgg});
        ret.add(results[0].getLabelPlural());
        List<Schema.ChildRelationship> crs = recordId.getSObjectType().getDescribe().getChildRelationships();
        for (Schema.ChildRelationship cr:crs)
        {
            if (cr.getChildSObject().getDescribe().getName()==nomeOgg && cr.getField().getDescribe().getName()==nomeCampoRelazione)
            {
                ret.add(cr.getRelationshipName());
                return ret;
            }
        }
        return null;
    }

    @AuraEnabled
    public static Map<String,Object> getPermissionsAndErrors(String nomeOggetto,String stringaCampi,String nomeCampoRelazione)
    {
        Map<String,Object> infoPermessiEdErrori = new Map<String,Object>();
        List<String> campi = stringaCampi.split(',');
        String query = 'SELECT Id';
        for (String nomeCampo:campi)
        {
            if (nomeCampo!='Id')
            {
                query += ',' + nomeCampo;
            }
        }
        
        query += ',' + nomeCampoRelazione;
        query += ' FROM ' + nomeOggetto;
        query += ' LIMIT 1';
        Boolean campiENomeOggValidi = true;
        try {
            List<SObject> lista = Database.query(query);
        } catch (QueryException e) {
            campiENomeOggValidi = false;
        }
        infoPermessiEdErrori.put('CampiENomeOggValidi',campiENomeOggValidi);
        if (campiENomeOggValidi) {
            Schema.DescribeSobjectResult res = Schema.describeSObjects(new List<String>{nomeOggetto})[0];
            List<Boolean> permessiCampi = new List<Boolean>();

            List<String> labelCampi = new List<String>();
            List<String> tipiCampi = new List<String>();
            Integer numCampiCreabili = 0;

            Map<String, Schema.SObjectField> infoCampi = res.fields.getMap();
            Map<String, Schema.DescribeSobjectResult> desSobjs = new Map<String, Schema.DescribeSobjectResult>();
            for (String nomeCampo:campi) {
                String path = '';
                Boolean percorsoNonAccessibile = false;
                if (nomeCampo.contains('.') )
                {
                    String copiaNomeCampo = nomeCampo;
                    List<Schema.sObjectType> oggCorrelati = null;
                    
                    while (copiaNomeCampo.contains('.') && !percorsoNonAccessibile)
                    {
                        String nomeCampoLookup = copiaNomeCampo.substringBefore('.');
                        if (nomeCampoLookup.endsWith('__r')) nomeCampoLookup = nomeCampoLookup.substringBefore('__r')+'__c';
                        else nomeCampoLookup+= 'Id';
                        if (oggCorrelati==null)
                            oggCorrelati = infoCampi.get(nomeCampoLookup).getDescribe().getReferenceTo();
                        else
                        {
                            for (Schema.sObjectType oggCorrelato:oggCorrelati)
                            {
                                path += oggCorrelato.getDescribe().getName()+'>';
                                Map<String, Schema.SObjectField> infoCampiTmp = oggCorrelato.getDescribe().fields.getMap();
                                if (infoCampiTmp.get(nomeCampoLookup)!=null)
                                {
                                    if (!infoCampiTmp.get(nomeCampoLookup).getDescribe().isAccessible())
                                    {
                                        permessiCampi.add(infoCampiTmp.get(nomeCampoLookup).getDescribe().isAccessible());
                                        labelCampi.add(path + infoCampiTmp.get(nomeCampoLookup).getDescribe().getLabel());
                                        tipiCampi.add(infoCampiTmp.get(nomeCampoLookup).getDescribe().getType().name());
                                        percorsoNonAccessibile = true;
                                    }
                                    oggCorrelati = infoCampiTmp.get(nomeCampoLookup).getDescribe().getReferenceTo();
                                    break;
                                }
                            }
                        }
                        copiaNomeCampo = copiaNomeCampo.substringAfter('.');
                    }
                    if (!percorsoNonAccessibile)
                    {
                        for (Schema.sObjectType oggCorrelato:oggCorrelati)
                        {
                            path += oggCorrelato.getDescribe().getName()+'>';
                            Map<String, Schema.SObjectField> infoCampiTmp = oggCorrelato.getDescribe().fields.getMap();
                            if (infoCampiTmp.get(copiaNomeCampo)!=null)
                            {
                                permessiCampi.add(infoCampiTmp.get(copiaNomeCampo).getDescribe().isAccessible());
                                if (infoCampiTmp.get(copiaNomeCampo).getDescribe().isCreateable()) numCampiCreabili++;
                                labelCampi.add(path + infoCampiTmp.get(copiaNomeCampo).getDescribe().getLabel());
                                tipiCampi.add(infoCampiTmp.get(copiaNomeCampo).getDescribe().getType().name());

                                break;
                            }
                        }
                    }
                }
                else
                {
                    labelCampi.add(infoCampi.get(nomeCampo).getDescribe().getLabel());
                    tipiCampi.add(infoCampi.get(nomeCampo).getDescribe().getType().name());

                    permessiCampi.add(infoCampi.get(nomeCampo).getDescribe().isAccessible());
                    if (infoCampi.get(nomeCampo).getDescribe().isCreateable()) numCampiCreabili++;
                }
            }
            infoPermessiEdErrori.put('Campi', permessiCampi);

            infoPermessiEdErrori.put('Label', labelCampi);
            infoPermessiEdErrori.put('Tipi', tipiCampi);
            System.debug(infoCampi.get('Name').getDescribe().isCreateable());
            //infoPermessiEdErrori.put('Oggetto', infoCampi.get('Name').getDescribe().isCreateable());
            infoPermessiEdErrori.put('Oggetto', (numCampiCreabili>0) );
        }
        return infoPermessiEdErrori;
    }

    @AuraEnabled
    public static String getIcon(String nomeOggetto)
    {
        SystemMode sm = new SystemMode();
        return sm.getIconSM(nomeOggetto);
    }

    @AuraEnabled
    public static List<String> getNamesFromId(List<String> reqName)
    {
        //nRecord,nomeCampo,Id
        List<String> oggettiLookup = new List<String>();
        Map<String,String> campoOggetto = new Map<String,String>();
        for (String s:reqName)
        {
            Id idReference = Id.valueOf(s.split(',')[2]);
            String nomeObj = idReference.getSObjectType().getDescribe().getName();
            if (campoOggetto.get(s.split(',')[1])==null)
                campoOggetto.put(s.split(',')[1],nomeObj);
        }
        Map<String,String> queryOggetto = new Map<String,String>();
        for (String s:reqName)
        {
            String[] p = s.split(',');
            String nRecord = p[0];
            String nomeCampo = p[1];
            String idRef = p[2];
            if (queryOggetto.get(campoOggetto.get(nomeCampo))==null)
            {
                String query = 'SELECT Name FROM '+campoOggetto.get(nomeCampo)+' WHERE Id IN (\''+idRef+'\',';
                queryOggetto.put(campoOggetto.get(nomeCampo),query);
            }
            else
            {
                String query = queryOggetto.get(campoOggetto.get(nomeCampo))+'\''+idRef+'\',';
                queryOggetto.put(campoOggetto.get(nomeCampo),query);
            }
        }
        Map<String,String> idConNomi = new Map<String,String>();
        for (String key:queryOggetto.keySet())
        {
            queryOggetto.put(key,queryOggetto.get(key).substringBeforeLast(',')+')');
            List<SObject> lista = Database.query(queryOggetto.get(key));
            for (SObject o:lista) idConNomi.put((String)o.get('Id'),(String)o.get('Name'));
        }
        List<String> ret = new List<String>();
        for (String s:reqName) {
            String[] p = s.split(',');
            String nRecord = p[0];
            String nomeCampo = p[1];
            String idRef = p[2];
            String str = nRecord+','+nomeCampo+','+idConNomi.get(idRef);
            ret.add(str);
        }
        return ret;
    }

    @AuraEnabled
    public static SObject deleteRecordById(Id idOgg)
    {
        String nomeObj = idOgg.getSObjectType().getDescribe().getName();
        String query = 'SELECT Id FROM '+nomeObj;
        query += ' WHERE Id=\''+idOgg+'\'';
        SObject ogg = Database.query(query);
        delete ogg;
        return ogg;
    }

    @AuraEnabled
    public static String getSessionIdAndInitPushTopic(String nomeOgg,String stringaCampi,String nomeCampoRelazione,String recordId,String nomePushTopic)
    {
        SystemMode sm = new SystemMode();
        return sm.getSessionIdAndInitPushTopicSM(nomeOgg,stringaCampi,nomeCampoRelazione,recordId,nomePushTopic);
    }

    @AuraEnabled
    public static List<Id> getAvailableRecordTypes(String nomeOgg)
    {
        List<Id> ids = new List<Id>();
        List<RecordTypeInfo> infos = Schema.describeSObjects(new List<String>{nomeOgg})[0].getRecordTypeInfos();
        if (infos.size() > 1) {
            for (RecordTypeInfo i : infos) {
                if (i.isAvailable() && !String.valueOf(i.getRecordTypeId()).endsWith('AAA'))
                    ids.add(i.getRecordTypeId());
            }
        } 
        return ids;
    }
}